// Copyright (c) 2014 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.

package org.cef.browser;

import org.cef.CefClient;
import org.cef.OS;
import org.cef.handler.CefWindowHandler;
import org.cef.handler.CefWindowHandlerAdapter;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.layout.Region;

import java.awt.Component;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.LongSupplier;
import javax.swing.Timer;

/**
 * This class represents a windowed rendered browser for JavaFX.
 * The visibility of this class is "package". To create a new
 * CefBrowser instance, please use CefBrowserFactory.
 */
class CefBrowserWrFX extends CefBrowserWr implements CefBrowserFX {
    private Rectangle content_rect_ = new Rectangle(0, 0, 0, 0);
    private long window_handle_ = 0;
    private boolean justCreated_ = false;
    private double scaleFactor_ = 1.0;
 
    private CefWindowHandlerAdapter win_handler_ = new CefWindowHandlerAdapter() {
        @Override
        public Rectangle getRect(CefBrowser browser) {
            synchronized (content_rect_) {
                return content_rect_;
            }
        }
    };

    CefBrowserWrFX(CefClient client, String url, CefRequestContext context) {
        this(client, url, context, null, null);
    }

    private CefBrowserWrFX(CefClient client, String url, CefRequestContext context,
            CefBrowserWrFX parent, Point inspectAt) {
        super(client, url, context);
    }
    
    @Override
    public Node getUIComponentFX(LongSupplier getWindowHandle) {
    	// ls: works for W10
    	if (!OS.isWindows()) {
    		return null;
    	}
    	if (window_handle_ != 0) {
    		return null;
    	}
    	Node browserNode = new Region();
    	Function<Boolean, Boolean> createBrowserIfRequired = new Function<Boolean, Boolean>() {
			@Override
			public Boolean apply(Boolean hasParent) {
	            if (isClosed()) return false;

	            long windowHandle = 0;
	            Component canvas = null;
	            if (hasParent) {
	            	window_handle_ = getWindowHandle.getAsLong();
	                windowHandle = getWindowHandle();
	            }

	            if (getNativeRef("CefBrowser") == 0) {
	                if (getParentBrowser() != null) {
	                    createDevTools(getParentBrowser(), getClient(), windowHandle, false, false, canvas,
	                            getInspectAt());
	                    return true;
	                } else {
	                    createBrowser(getClient(), windowHandle, getUrl(), false, false, canvas,
	                            getRequestContext());
	                    return true;
	                }
	            } else if (hasParent && justCreated_) {
	                setParent(windowHandle, canvas);
	                setFocus(true);
	                justCreated_ = false;
	            }

	            return false;
			}
		};
    	Consumer<Void> doUpdateFX = new Consumer<Void>() {
			@Override
			public void accept(Void t) {
			    if (isClosed()) return;
				
			    Bounds vr = browserNode.getBoundsInLocal();
			    Rectangle clipping = new Rectangle((int) (vr.getMinX() * scaleFactor_),
			            						   (int) (vr.getMinY() * scaleFactor_),
			            						   (int) (vr.getWidth()* scaleFactor_),
			            						   (int) (vr.getHeight() * scaleFactor_));
			
			    synchronized (content_rect_) {
			    	content_rect_ = clipping;
			    	updateUI(clipping, content_rect_);
			    }
			}
		};
       	Timer delayedUpdate_ = new Timer(100, null);
        	ActionListener delayedUpdateAction_ = new ActionListener() {
    		@Override
    		public void actionPerformed(ActionEvent e) {
    			Platform.runLater(new Runnable() {
    				@Override
    				public void run() {
    					if (isClosed()) return;
    					
    					boolean hasCreatedUI = createBrowserIfRequired.apply(true);
    					
    					if (hasCreatedUI) {
    						delayedUpdate_.restart();
    					}
    				}
    			});
    		}
    	};
      	delayedUpdate_.addActionListener(delayedUpdateAction_);
    	delayedUpdate_.setRepeats(false);
		browserNode.boundsInParentProperty().addListener(new ChangeListener<Bounds>() {
			@Override
			public void changed(ObservableValue<? extends Bounds> observable, Bounds oldValue, Bounds newValue) {
				wasResized((int)newValue.getWidth(), (int)newValue.getHeight());
				doUpdateFX.accept(null);
			}
		});
		browserNode.scaleXProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				scaleFactor_ = newValue.doubleValue();
			}
		});
        delayedUpdate_.restart();
        return browserNode;
    }

    @Override
    public CefWindowHandler getWindowHandler() {
        return win_handler_;
    }

    private synchronized long getWindowHandle() {
        return window_handle_;
    }

}
