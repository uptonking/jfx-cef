// Copyright (c) 2013 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.

package org.cef.browser;

import org.cef.handler.CefWindowHandler;
import javafx.scene.Node;
import java.util.function.LongSupplier;

/**
 * Interface representing a browser for JavaFX.
 */
public interface CefBrowserFX extends CefBrowser {

    /**
     * Get an implementation of CefWindowHandler if any.
     * @return An instance of CefWindowHandler or null.
     */
    public CefWindowHandler getWindowHandler();
    
    /**
     * Get the underlying UI component
     * @return The underlying UI component.
     */
    public Node getUIComponentFX(LongSupplier getWindowHandle);

}